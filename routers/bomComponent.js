const express = require("express")
const bomControllers = require("../controllers/bomControllers")
const router = express.Router()
// 二级路由 
// 访问/allComponents/all
router.route("/all").get(bomControllers.getAllComponents)
module.exports = router