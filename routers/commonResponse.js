const express = require("express")
const multer  = require('multer')
const commonControllers = require('../controllers/commonControllers')
const upload = multer({ dest: 'public/upload/' })
const router = express.Router()

// 二级路由 
router.post('/single',upload.single('avatar'),commonControllers.uploadFile)
module.exports = router