exports.getAllComponents = async (req, res) => {
  // 读取BOM菜单
  const bomConfigs = require('../configs/bom.config.json')
  try {
    res.status(200).json({
      status: "success",
      data: bomConfigs,
    })
  } catch (err) {
    res.status(400).json({
      status: "fail",
      message: err.message,
    })
  }
}