const fs = require('fs')

exports.uploadFile = async (req,res,next)=>{
  try {
    console.log(req.file.originalname);
    fs.rename(`public/upload/${req.file.filename}`,`public/upload/${req.file.originalname}`,(err)=>{return})
    res.status(201).json({
      status: 'success',
      data: {
        url: `http://localhost:4567/static/${req.file.originalname}`
      }
    })
  } catch (err) {
    console.log(err)
  }
}