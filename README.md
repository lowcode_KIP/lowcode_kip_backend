# 知识就是力量_字节青训营_后端(express)
## 安装依赖 & 启动服务
```bash
yarn;yarn start
```
## 环境
端口号位于 `config.env` 文件下 `PORT` 处声明  
## 组件
- 所有组件位于 `configs/bom.config.json`
## 路由
### 组件
GET: `localhost:port/allComponents/all` 返回全部组件  
### 文件服务
POST: `localhost:port/common/single` 单文件上传, 返回文件地址  
eg:  
```html
<form 
action="http://localhost:port/common/single" 
method="post" 
enctype="multipart/form-data" 
>
  <input type="file" name="avatar" />
  <button type="submit">sc</button>
</form>
```