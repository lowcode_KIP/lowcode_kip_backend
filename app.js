const express = require("express")
const bomComponent = require("./routers/bomComponent")
const commonResponse = require("./routers/commonResponse")
const app = express()

// 跨域请求
app.use((req, res, next) => {
  res.header("Access-Control-Allow-Origin", "*")
  // 允许前端axios请求
  res.header(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept"
  )
  res.header(
    "Access-Control-Allow-Methods",
    "PUT,POST,PATCH,GET,DELETE,OPTIONS"
  )
  res.header("Content-Type", "application/json;charset=utf-8")
  next()
})
app.use('/static', express.static('public/upload'))
app.use("/allComponents", bomComponent)
app.use("/common", commonResponse)
module.exports = app