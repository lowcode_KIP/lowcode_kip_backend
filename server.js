const dotenv = require("dotenv")
dotenv.config({
  path: "./config.env",
})
const app = require("./app")
// 开始监听端口
const DB_PORT = process.env.PORT
app.listen(DB_PORT, () => {
  console.log(
    `开始监听端口 ${DB_PORT} 你可以打开http://localhost:${DB_PORT}`
  )
})